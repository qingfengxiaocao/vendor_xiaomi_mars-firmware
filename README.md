# android_vendor_xiaomi_mars-firmware

Firmware images for Xiaomi 11 Pro (mars), to include in custom ROM builds.

**Current version**: fw_star_miui_STAR_V13.0.12.0.SKACNXM_3d5b1d0e78_12.0.zip

### How to use?

1. Clone this repo to `vendor/xiaomi/mars-firmware`

2. Include it from `BoardConfig.mk` in device tree:

```
# Firmware
-include vendor/xiaomi/mars-firmware/BoardConfigVendor.mk
```
